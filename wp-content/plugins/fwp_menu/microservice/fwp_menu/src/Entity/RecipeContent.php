<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RecipeContentRepository")
 */
class RecipeContent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Serving", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $serving;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $calories;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $protein;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $carbs;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $fat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Recipe", inversedBy="contents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $recipe;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServing(): ?Serving
    {
        return $this->serving;
    }

    public function setServing(Serving $serving): self
    {
        $this->serving = $serving;

        return $this;
    }

    public function getCalories()
    {
        return $this->calories;
    }

    public function setCalories($calories): self
    {
        $this->calories = $calories;

        return $this;
    }

    public function getProtein()
    {
        return $this->protein;
    }

    public function setProtein($protein): self
    {
        $this->protein = $protein;

        return $this;
    }

    public function getCarbs()
    {
        return $this->carbs;
    }

    public function setCarbs($carbs): self
    {
        $this->carbs = $carbs;

        return $this;
    }

    public function getFat()
    {
        return $this->fat;
    }

    public function setFat($fat): self
    {
        $this->fat = $fat;

        return $this;
    }

    public function getRecipe(): ?Recipe
    {
        return $this->recipe;
    }

    public function setRecipe(?Recipe $recipe): self
    {
        $this->recipe = $recipe;

        return $this;
    }
}
