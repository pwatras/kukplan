<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IngredientRepository")
 */
class Ingredient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Unit", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $unit;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ingredient", mappedBy="ingredient")
     */
    private $substitutes;

    public function __construct()
    {
        $this->substitutes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUnit(): ?Unit
    {
        return $this->unit;
    }

    public function setUnit(Unit $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * @return Collection|Ingredient[]
     */
    public function getSubstitutes(): Collection
    {
        return $this->substitutes;
    }

    public function addSubstitute(Ingredient $substitute): self
    {
        if (!$this->substitutes->contains($substitute)) {
            $this->substitutes[] = $substitute;
            $substitute->setIngredient($this);
        }

        return $this;
    }

    public function removeSubstitute(Ingredient $substitute): self
    {
        if ($this->substitutes->contains($substitute)) {
            $this->substitutes->removeElement($substitute);
            // set the owning side to null (unless already changed)
            if ($substitute->getIngredient() === $this) {
                $substitute->setIngredient(null);
            }
        }

        return $this;
    }
}
