<?php

namespace App\Repository;

use App\Entity\RecipeContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RecipeContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method RecipeContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method RecipeContent[]    findAll()
 * @method RecipeContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecipeContentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RecipeContent::class);
    }

//    /**
//     * @return RecipeContent[] Returns an array of RecipeContent objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RecipeContent
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
