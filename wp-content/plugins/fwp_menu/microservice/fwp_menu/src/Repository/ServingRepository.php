<?php

namespace App\Repository;

use App\Entity\Serving;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Serving|null find($id, $lockMode = null, $lockVersion = null)
 * @method Serving|null findOneBy(array $criteria, array $orderBy = null)
 * @method Serving[]    findAll()
 * @method Serving[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Serving::class);
    }

//    /**
//     * @return Serving[] Returns an array of Serving objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Serving
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
