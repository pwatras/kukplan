<?php

namespace App\Repository;

use App\Entity\PreparationSteps;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PreparationSteps|null find($id, $lockMode = null, $lockVersion = null)
 * @method PreparationSteps|null findOneBy(array $criteria, array $orderBy = null)
 * @method PreparationSteps[]    findAll()
 * @method PreparationSteps[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PreparationStepsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PreparationSteps::class);
    }

//    /**
//     * @return PreparationSteps[] Returns an array of PreparationSteps objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PreparationSteps
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
