<?php

namespace App\Repository;

use App\Entity\MealGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MealGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method MealGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method MealGroup[]    findAll()
 * @method MealGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MealGroupRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MealGroup::class);
    }

//    /**
//     * @return MealGroup[] Returns an array of MealGroup objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MealGroup
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
