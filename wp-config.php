<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


if(file_exists(__DIR__.'/fwpconfig.php')) { include __DIR__.'/fwpconfig.php'; };
/** The name of the database for WordPress */

if(!defined('DB_NAME')): 
    define('DB_NAME', 'cookplan_wp');
endif;
/** MySQL database username */
if(!defined('DB_USER')): 
    define('DB_USER', 'cookplan');
endif;
/** MySQL database password */
if(!defined('DB_PASSWORD')): 
    define('DB_PASSWORD', 'n8t3rSYnZbut4aYR');
endif;
/** MySQL hostname */
if(!defined('DB_HOST')): 
    define('DB_HOST', 'localhost');
endif;
/** Database Charset to use in creating database tables. */
if(!defined('DB_CHARSET')): 
    define('DB_CHARSET', 'utf8mb4');
endif;
/** The Database Collate type. Don't change this if in doubt. */
if(!defined('DB_COLLATE')): 
    define('DB_COLLATE', '');
endif;


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~$~1SFV (MN7oci>C5X~MtQB62U>7vA%nGm(zicz$D^7%hj(yO5Z[{&-1mpD?,lq');
define('SECURE_AUTH_KEY',  '<:@+*q~e JG`y6zqyTXEn4f.vzaD1z{Jas)bO/4~mSe|/55cX-8Jljo+A-6[[v$d');
define('LOGGED_IN_KEY',    'XP1N#mP3G6+t-QsCjuTZuZ$YS:Bmf_Z9M=cTCH5 ,&_nLf6Fq]s}|Tux{^nj$#Pn');
define('NONCE_KEY',        '#}tKLze-17M:PpA,s5.B4?Rwi+Rd^Bzf. k$JTe}4+wIk]e58>:ff[--k?,%$D)o');
define('AUTH_SALT',        'QboAh!;qE{?Db_mT5jZPa>8q9v&gL^~|bUqH6) zv+9da6<@4VN7^BH>55&35&g0');
define('SECURE_AUTH_SALT', 'OHRBe#qa3{a~~0E:,D&%M3UyuanF!7tCw0?85$P4c&$f`8(0:fP_uJY$u`r$gb/E');
define('LOGGED_IN_SALT',   'W*+2Q$L8t9(kk)^N_!UqnB_vNl?)+MZ)r4y.L#-_2r{bIBRb^za;#Go^$7YO|9Sz');
define('NONCE_SALT',       'Joz<FUyz&)rEu_DOPTjhF7L)H7ccO6%kxP^;Nw&`~M!R7.77qI k@4->c7WbNt_ ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
